import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  toDoArray = [];
  doneArray = [];
  toDoItem = '';

  constructor(public navCtrl: NavController) {

  }

  addItem() {
    if (this.toDoItem !== '') {
      this.toDoArray.push(this.toDoItem);
    }
    this.toDoItem = '';
  }

  finishItem(index) {
    let doneItem = this.toDoArray[index];
    this.toDoArray.splice(index, 1);
    this.doneArray.push(doneItem);
  }

  unfinishItem(index) {
    let toDoAgainItem = this.doneArray[index];
    this.doneArray.splice(index, 1);
    this.toDoArray.push(toDoAgainItem);
  }

  deleteItemFromToDo(index) {
    this.toDoArray.splice(index, 1);
  }

   deleteItemFromDone(index) {
    this.doneArray.splice(index, 1);
  }
}
